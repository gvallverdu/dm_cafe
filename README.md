# DM_cafe

Germain Salvato-Vallverdu - germain.vallverdu@univ-pau.fr

Présentation de la dynamique moléculaire au travers d'exemples simples et 
d'un exercice classique de mécanique du point. L'exemple choisi est le calcul
de la trajectoire d'une balle de baseball. Plusieurs modèles sont envisagés en
prenant ou non en compte une force de traînée.

## Contenu

* **slides/** : slides de la présentation
* **simulations/** : fichiers d'entrée et de sortie des simulations
    * du méthane à plusieurs température
    * du toluène
    * du metazachlor, simulation reactive, ReaxFF
* **notebooks/** : python notebooks
    * pour le calcul des trajectoires de la balle de baseball
    * pour produire des figures à partir des simulations du méthane
    * pour travailler sur une marche aléatoire en comparaison avec Scratch
    * pour visualiser une simulation réactive

## Licence

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br /></a>

Tout le materiel est mis à disposition selon les termes de la
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">licence
Creative Commons Attribution -  Partage dans les Mêmes
Conditions 4.0 International</a>.
