#!/bin/bash

# Minimization
# ------------

## steepest descent

gmx grompp -f min_steep.mdp -c box.gro -p topol.top -o min_steep.tpr
gmx mdrun -s min_steep.tpr -c confout_steep.gro -g min_steep.log -e ener_min.edr

## conjugate grandient

#gmx grompp -f min_cg.mdp -c confout_steep.gro -p topol.top -o min_cg.tpr
#gmx mdrun -s min_cg.tpr -c confout_cg.gro -g min_cg.log

echo "              Minimization Done ! -----------------------------"

# equilibration
# -------------

## NVT

gmx grompp -f nvt_eq.mdp -c confout_steep.gro -p topol.top -o nvt_eq.tpr
gmx mdrun -s nvt_eq.tpr -g nvt_eq.log -c confout_nvt_eq.gro -e ener_nvt_eq.edr -x traj_comp_eq_nvt.xtc -o traj_nvt_eq.trr

## NPT

gmx grompp -f npt_eq.mdp -c confout_nvt_eq.gro -p topol.top -o npt_eq.tpr
gmx mdrun -s npt_eq.tpr -g npt_eq.log -c confout_npt_eq.gro -e ener_npt_eq.edr -x traj_comp_npt_eq.xtc -o traj_npt_eq.trr

echo "              Equilibration Done ! ----------------------------"

# run simulation
# --------------

echo ""
echo "          START SIMULATION "
echo ""

gmx grompp -f run_npt.mdp -c confout_npt_eq.gro -p topol.top -o run_npt.tpr
gmx mdrun -s run_npt.tpr -g run_npt.log

gmx trjconv -pbc mol -center -ur compact -s run_npt.tpr -f traj.trr -o trajout.xtc
