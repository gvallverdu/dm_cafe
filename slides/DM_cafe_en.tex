%\pdfminorversion=4
\documentclass[8pt, english]{beamer}

\usepackage[T1]{fontenc} 
\usepackage[utf8]{inputenc}
\usepackage{babel}
    
% titre et informations de la présentation
\title[Numerical simulations]{Computational chemistry and numerical simulations}
\author[Germain Salvato-Vallverdu]{Germain Salvato-Vallverdu\\ \vskip3mm\texttt{germain.vallverdu@univ-pau.fr}}
\date{\today}
\institute[IPREM]{Institut des Sciences Analytiques et de Physico-Chimie pour l'Environnement et les Matériaux}

% chargement du theme
\usetheme[color=Ubleu, outline=plan]{UPPA2014}
%\usetheme[color=Ubleu, outline=contents, hideothersubsections]{UPPA2014}
%\usetheme{UPPA2014}
%\usetheme[width=5cm]{UPPA2014}
%\usetheme[outline]{UPPA2014}
%\usetheme[outline, hideothersubsections]{UPPA2014}
%\usetheme[outline, hideallsubsections]{UPPA2014}

\newcommand{\cbox}[2]{\parbox{#1\textwidth}{\centering #2}}

\usepackage{siunitx}
\usepackage{tikz}
\usepackage{amsmath}
\usetikzlibrary{patterns,decorations.pathreplacing,calc,decorations.markings,shapes}
%\usetikzlibrary{}%,shapes,patterns,arrows,3d,shadows}

\usepackage{minted}
\usepackage{mes_macros}

\graphicspath{{img/}}

\renewcommand{\footnoterule}{}

\definecolor{redF}{rgb}{0.81, 0.00, 0.00}
\definecolor{bleuP}{rgb}{0.02, 0.38, 0.67}


\AtBeginEnvironment{block}{
    \setbeamercolor{description item}{fg=Ubleu}
    \setbeamercolor{itemize item}{fg=Ubleu}
    \setbeamercolor{itemize subitem}{fg=Uviolet}
}

\AtBeginEnvironment{exampleblock}{
    \setbeamercolor{description item}{fg=Uorange}
    \setbeamercolor{itemize item}{fg=Uorange}
    \setbeamercolor{itemize subitem}{fg=Uviolet}
}

\setbeamercolor{block title}{fg=white, bg=Ubleu}
\setbeamercolor{block title alerted}{fg=white,bg=rouge}
\setbeamercolor{block title example}{fg=white,bg=Uorange}
 
\setbeamercolor{block body}{bg=Ubleu!5}
\setbeamercolor{block body alerted}{bg=rouge!5}
\setbeamercolor{block body example}{bg=Uorange!5}

\setbeamerfont*{structure}{size=\large, series=\bfseries}

\begin{document}

\begin{frame}
    \titlepage
\end{frame}


\section*{Contents}

\tocframe{Outline}

\section[Comp. Chem.]{Computational Chemistry and Numerical Simulations}

\begin{frame}
    \frametitle{Computational Chemistry and Numerical Simulations}
    \framesubtitle{A multi-disciplinary domain}
    
    \centering
    \includegraphics[width=.9\textwidth]{CTh_sim}
    
\end{frame}

\begin{frame}
    \frametitle{Examples of research topics}
    \framesubtitle{May concern all disciplinary fields}

    \structure{Personal examples}

    \vspace{-1cm}
    \begin{center}
    \includegraphics[width=\textwidth]{sim_exemples}    
    \end{center}
    
    \structure{Various topics}
    
    \begin{itemize}
        \item Molecules, materials
        \item Biology, lithium batteries, energetic materials, petroleum fluids
        \item Quantum chemistry, molecular mechanics, mesoscopics models
    \end{itemize}

\end{frame}


\section[Baseball]{Projectile trajectory}

\subsection[Free fall]{Model without friction - Free fall}

\begin{frame}
    \frametitle{Projectile trajectory}
    \framesubtitle{Classical example of point mechanics exercise}

    \begin{minipage}[t]{.59\textwidth}
        \structure{Solution: simple analytic resolution}
        
        We consider:
        \begin{itemize}
            \item a point of mass $m$ 
            \item a land-based referential $\mathcal{R}$, supposed Galilléen.
            \item The unique force acting on the system is the gravity $\vec{P} = m\vec{g}$.
        \end{itemize}
%        
        \begin{equation*}
            m\vec{a}  = \vec{P} = m\vec{g} \\
        \end{equation*}
        \begin{equation*}
            \begin{cases}
                a_x & = 0 \vspace{1ex}\\
                a_z & = -g 
            \end{cases}
            \qquad
            \begin{cases}
                \displaystyle\frac{dv_x}{dt} & = 0 \vspace{1ex}\\
                \displaystyle\frac{dv_z}{dt} & = -g
            \end{cases}
        \end{equation*}

        \uncover<2>{
        With $\alpha$ the angle between the initial velocity and the $(Ox)$ axes. With $x_o = 0$, we get:
%
        $$z = -\frac{gx^2}{2v_o^2\cos^2\alpha} + x \tan\alpha + z_o$$    
        }
   
    \end{minipage}
    \begin{minipage}[t]{.4\textwidth}
        \centering
        \structure{Graphic}
        
        \only<1>{\includegraphics[width=\textwidth]{baseball0}}
        \only<2>{\includegraphics[width=\textwidth]{baseball1}}
    \end{minipage}

\end{frame}

\subsection[Stockes]{Model with friction - simple case}

\begin{frame}
    \frametitle{Projectile trajectory}
    \framesubtitle{Classical example of point mechanics exercise}
    
    \begin{minipage}[t]{.59\textwidth}
        \structure{Solution: with air/fluid resistance}
        
        we consider
        \begin{itemize}
            \item a sphere of mass $m$ and diameter $D$.
            \item a land-based referential $\mathcal{R}$, supposed Galilléen.
            \item The forces acting on the system are:
            \begin{itemize}
                \item the gravity $\vec{P} = m\vec{g}$
                \item the drag force $\vec{F}_T = - k\vec{v}$\\
                $k = 3\pi\eta D$
            \end{itemize}            
        \end{itemize}
%        
        \begin{equation*}
            m\vec{a}  = \vec{P} + \vec{F}_T = m\vec{g} -k \vec{v}
        \end{equation*}
        \begin{equation*}
            \begin{cases}
                \displaystyle a_x & = -\frac{k}{m} v_x  \vspace{1ex} \\
                \displaystyle a_z & = -g - \frac{k}{m} v_z  
            \end{cases}
            \qquad
            \begin{cases}
                \displaystyle \frac{dv_x}{dt} + \frac{k}{m} v_x & = 0 \vspace{1ex}\\
                \displaystyle \frac{dv_z}{dt} + \frac{k}{m} v_z & = -g 
            \end{cases}
        \end{equation*}

        \uncover<2>{
        Solution:
        \begin{equation*}
            \begin{cases}
                \displaystyle x(t) & = \displaystyle \frac{m}{k} v_x^{(o)} \left(1-\exp\left(-\frac {k}{m}t\right)\right) \vspace{1ex} \\
                \displaystyle y(t) & = \displaystyle - \frac{mg}{k} t + \frac{m}{k}\left(v_y^{(o)}+ \frac {mg}{k}\right)\left(1-\exp\left(-\frac {k}{m}t\right)\right)
            \end{cases}
        \end{equation*}
        }
   
    \end{minipage}
    \begin{minipage}[t]{.4\textwidth}
        \centering
        \structure{Graphic}
        
        \only<1>{\includegraphics[width=\textwidth]{baseball1}}
        \only<2>{\includegraphics[width=\textwidth]{baseball2}}
        
        \bigskip
        
        Stockes flow case $R_e << 1$
    \end{minipage}

\end{frame}

\subsection[Turbulent]{Model with a drag force - turbulent case}

\begin{frame}
    \frametitle{Projectile trajectory}
    \framesubtitle{Classical example of point mechanics exercise}

    \begin{minipage}[t]{.59\textwidth}
        \structure{Solution: with air/fluid resistance}
        
        We consider:
        \begin{itemize}
            \item a sphere of mass $m$ and diameter $D$.
            \item a land-based referential $\mathcal{R}$, supposed Galilléen.
            \item The forces acting on the system are:
            \begin{itemize}
                \item the gravity $\vec{P} = m\vec{g}$
                \item the drag force $\displaystyle \vec{F}_T = - \frac{1}{2} \rho S C_x v\vec{v}$
                \item Archimedes principle $\vec{\Pi} = -\rho V \vec{g}$
            \end{itemize}            
        \end{itemize}
%        
        \begin{equation*}
            m\vec{a}  = \vec{P} + \vec{F}_T + \vec{\Pi} = m\vec{g} 
            - \frac{1}{2} \rho S C_x v \vec{v}
            -\rho V \vec{g}
        \end{equation*}
        \begin{equation*}
            \begin{cases}
                \displaystyle a_x = \frac{dv_x}{dt} =  -\frac{\rho S C_x}{2m} v \, v_x
                \vspace{1ex}\\
                \displaystyle a_z = \frac{dv_z}{dt} = -g + \frac{\rho V g}{m} - \frac{\rho S C_x}{2m} v \, v_y 
            \end{cases}
        \end{equation*}

        \uncover<2>{
        {\setbeamercolor{block body alerted}{fg=white, bg=rouge}
        \begin{alertblock}{}
            \vspace{-2mm}
            \centering\bfseries
            No simple analytic solution
        \end{alertblock}}
        }
   
    \end{minipage}
    \begin{minipage}[t]{.4\textwidth}
        \centering
        \structure{Graphic}
        
        \only<1>{\includegraphics[width=\textwidth]{baseball2}}
        \only<2>{\includegraphics[width=\textwidth]{baseball3}}
        
        \bigskip
        
        Turbulent flow case $R_e > 2000$\\
        (baseball $R_e$ = \num{2.1e5})
    \end{minipage}

\end{frame}

\subsection[Numerical App.]{Numerical resolution of the system}

\begin{frame}
    \frametitle{Projectile trajectory}
    \framesubtitle{Numerical integration}

    \structure{Euler approach}
    
    Discretization of time using a Taylor series
    
    Velocities :
    \begin{align*}
        v_x(t + \delta t) & = v_x(t) + \frac{dv_x}{dt} \delta t + \mathcal{O}(\delta t^2)  &
        v_x(t + \delta t) & = v_x(t) + a_x(t) \delta t  + \mathcal{O}(\delta t^2) \\
        v_z(t + \delta t) & = v_z(t) + \frac{dv_z}{dt} \delta t + \mathcal{O}(\delta t^2) &
        v_z(t + \delta t) & = v_z(t) + a_z(t) \delta t  + \mathcal{O}(\delta t^2)
    \end{align*}
%
    Positions:
%
    \begin{align*}
        x(t + \delta t) & = x(t) + \frac{dx}{dt} \delta t + \mathcal{O}(\delta t^2) &
        x(t + \delta t) & = x(t) + v_x(t) \delta t + \mathcal{O}(\delta t^2)\\
        z(t + \delta t) & = z(t) + \frac{dz}{dt} \delta t + \mathcal{O}(\delta t^2)  &
        z(t + \delta t) & = z(t) + v_z(t) \delta t + \mathcal{O}(\delta t^2)
    \end{align*}

    \structure{Implementation}
    \begin{itemize}
        \item Initial conditions: positions, velocities
    \end{itemize}
    
    for each time step:
    \begin{enumerate}
        \item Compute forces
        \item update velocities
        \item update positions
    \end{enumerate}
    
    \medskip
    Key parameters: $\delta t$, forces (contain the physics)

\end{frame}

\begin{frame}[fragile]
    \frametitle{Projectile trajectory}
    \framesubtitle{Numerical integration - A python program}

    \structure{Implémentation en python}
    
    \begin{minted}[bgcolor=black!5]{python}
# initial conditions
positions = np.zeros((data.nstep, 2))
velocities = np.zeros((data.nstep, 2))
positions[0] = [0, zo]
velocities[0] = [vo * np.cos(alpha), vo * np.sin(alpha)]
    
# run
for step in range(1, data.nstep):
    # compute forces
    forces = compute_forces(positions[step - 1], velocities[step - 1])
    
    # update velocities
    velocities[step] = velocities[step - 1] + forces * data.dt / data.m
    
    # update positions
    positions[step] = positions[step - 1] + velocities[step] * data.dt
    \end{minted}
    
    \begin{minipage}[c]{.78\textwidth}
    \structure{Python}
    
    \begin{itemize}
        \item Simple syntax, readable, easy to use, cross-platform
        \item High level language
        \item Huge library, active community, huge number of resources in all domains.
        \item Open source and free
    \end{itemize}
    \end{minipage}    
    \begin{minipage}[c]{.2\textwidth}
    \includegraphics[width=\textwidth]{snake}
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Projectile trajectoire}
    \framesubtitle{Numerical integration results}
    
    \begin{minipage}[t]{.49\textwidth}
    \centering
    \structure{time step effects}
    
    \includegraphics[width=\textwidth]{baseball4}
    \end{minipage}
    \begin{minipage}[t]{.49\textwidth}
    \centering
    \structure{Comparison}
    
    \includegraphics[width=\textwidth]{baseball3}
    \end{minipage}

\end{frame}

\section[Methane]{Numerical simulations of methane}

\subsection[Stat.Mech.]{Statistical thermodynamics}

\begin{frame}
    \frametitle{Classical mechanics and statistical mechanics}
    \framesubtitle{Build an opinion poll of chemical-physics}
    
    \vspace{-4.5mm}
    \begin{tikzpicture}[>=stealth, scale=1]
        \pgfmathsetseed{1408}

        \tikzstyle{part} = [circle, fill, color=rouge, draw=black!80, line width=.5pt, scale=.7]
        \draw[white] (-1, -6.5) rectangle (11.2, 2.8);
        \def\nat{100}

%        \draw<1>[line width=2pt, rounded corners, fill=black!5] (0, 0) rectangle ++ (2, 2);
%        \foreach \n in {1, 2, ..., \nat} {
%                \pgfmathsetmacro{\rx}{{1.8 * rnd + .1}}
%                \pgfmathsetmacro{\ry}{{1.8 * rnd + .1}}
%                \node<1>[part] at (\rx, \ry) {};
%        }        
        
        \foreach \x in {0, 1, ..., 4} {
            \draw<2->[line width=2pt, rounded corners, fill=black!5] (2.2 * \x, 0) rectangle ++ (2, 2);
            \foreach \n in {1, 2, ..., \nat} {
                \pgfmathsetmacro{\rx}{{1.8 * rnd + .1 + 2.2 * \x}}
                \pgfmathsetmacro{\ry}{{1.8 * rnd + .1}}
                \node<2->[part] at (\rx, \ry) {};
            }
        }
        
        \foreach \y in {0, -1, -2} {
            \draw<2->[line width=2pt, rounded corners, fill=black!5]
                (0, 2.15 * \y - .15 ) rectangle ++ (2, -2);
            \foreach \n in {1, 2, ..., \nat} {
                \pgfmathsetmacro{\rx}{{1.8 * rnd + .15}}
                \pgfmathsetmacro{\ry}{{1.8 * rnd + .1 + 2.15 * (\y - 1)}}
                \node<2->[part] at (\rx, \ry) {};
            }
        }
    
        \draw<2>[->, line width=2pt] (0, 2.25) -- (11, 2.25) 
            node[above, pos=1, font=\large\bfseries] {t};
        \draw<2->[->, line width=2pt] (0, 2.25) -- (11, 2.25) 
            node[above, pos=.5, font=\large] {\textbf{time average} (Boltzmann average)}
            node[above, pos=1, font=\large\bfseries] {t};
        \draw<2->[<->, line width=2pt] (-.25, 2) -- (-.25, -6.5) 
            node[above, pos=.5, rotate=90, font=\large] {\textbf{ensemble average} (Gibbs average)};
        
        \node<3->[rounded corners=2pt, rectangle, align=center, thick, inner sep=5pt,
              text width=7cm, right, color=white, fill=Ubleu] at (3, -5.5) {
%        \setlength{\baselineskip}{15pt}
        \textbf{Ergodique hypothesis:}\\
        \textit{An ensemble average and a time average are assumed to be equal
        when the number of configurations tends to infinity.}\par
        };
        
        \node<1->[right, text width=9.5cm] at (2.2, -2.3) {
        \structure{Statistical physics or Mechanics}
        \begin{itemize}
            \item A macroscopic state corresponds to an ensemble of microscopic states.
            \item At equilibrium, thermodynamics quantities fluctuate around an equilibrium value.
            \item An observable quantity is computed from \textit{an ensemble average}
%            \item \textbf{Time average:} Molecular dynamic simulations
%            \item \textbf{Ensemble average:} Monte Carlo simulations
        \end{itemize}
        \vskip-1ex
        \begin{align*}\small
            \left\langle A \right\rangle & = \frac{1}{N} \sum_{r_i, p_i} A(r_i, p_i) &
            \left\langle \left(A - \left\langle A \right\rangle \right)^2 \right\rangle & = 
            \left\langle A^2 \right\rangle - \left\langle A  \right\rangle^2
        \end{align*}
        \vskip-1ex
        One way to obtain an ensemble of configurations of a system is to compute
        the trajectory of the atoms composing the system.
        };
    \end{tikzpicture}

\end{frame} 
    
\subsection[Model]{The model}

\begin{frame}
    \frametitle{How to make a simulation of methane ?}
    \framesubtitle{A quick overview}
    
    \begin{minipage}[c]{.58\textwidth}
    \structure{A model for the interactions}

    Lenard-Jones potential (6-12):
    \begin{equation*}
        E(r) = 4\varepsilon\left(\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^6\right)
    \end{equation*}
    %
    The molecule is represented by a sphere:\\
    \begin{minipage}[c]{.49\textwidth}
    United atom model
    \begin{itemize}
        \item Rayon R = 3.73\AA
        \item $\varepsilon$ = 0.29 \si{\kilo cal\per\mol}
    \end{itemize}
    \end{minipage}
    \begin{minipage}[c]{.49\textwidth}
    \centering
    \includegraphics[width=.5\textwidth]{methane_UA}
    \end{minipage}

    \end{minipage}    
    \begin{minipage}[c]{.4\textwidth}
    \includegraphics[width=\textwidth]{potLJ}
    \end{minipage}

    \bigskip
    \begin{minipage}[t]{.49\textwidth}
    \structure{Periodic conditions}
    
    \includegraphics[width=\textwidth]{periodic_bound}
    \end{minipage}
    \begin{minipage}[t]{.49\textwidth}
    \structure{Numerical integration scheme}

    Verlet algorithm:
    %
    \begin{align*}
        \vec{r}(t + \delta t) & = 2 \vec{r}(t) - \vec{r}(t - \delta t) + \delta t^2 \vec{a}(t) \\
        \vec{v}(t) & = \frac{1}{2\delta t}\left(\frac{}{}\vec{r}(t+\delta t) - \vec{r}(t - \delta t)\right)
    \end{align*}
    %
    Acceleration is computed from the forces and thus from the model.

    \end{minipage}
\end{frame}

\subsection[Results]{Some result about methane simulations}

\begin{frame}
    \frametitle{Methane simulations}
    \framesubtitle{Some result and thermodynamic quantities}

    \begin{center}
        \includegraphics[width=.9\textwidth]{sim300K}
    \end{center}
    
    \begin{itemize}
        \item Equilibrium state: Fluctuations around an equilibrium value
        \item T = 300 K, P = 40 atm.
        \item (NPT) simulations: number of atoms, pressure and temperature are constant
    \end{itemize}

\end{frame}

\begin{frame}
    \frametitle{Methane simulations}
    \framesubtitle{Liquid, gaseous or supercritical ?}
    
    \begin{minipage}[t]{.4\textwidth}\centering
    \structure{Radial pair distribution function}
    
    \includegraphics[width=\textwidth]{rdf_methane}
    \end{minipage}
    \begin{minipage}[t]{.49\textwidth}\centering
    
    \structure{Visualization}
    
%    \begin{minipage}[t]{.3\textwidth}\centering
%    \textbf{Liquide}
%    
%    \includegraphics[width=2cm]{Meth_liquid}
%
%    \includegraphics[width=\textwidth]{legion_romaine}
%    \end{minipage}
%    \begin{minipage}[t]{.68\textwidth}\centering
%    \textbf{Gaz}
%    
%    \includegraphics[width=5cm]{Meth_gaz}
%    
%    \includegraphics[width=\textwidth]{bagarre}
%    \end{minipage}
    
    \bigskip
    P = 40 atm    
    
    \begin{minipage}[t]{.325\textwidth}\centering
    \textbf{Solide}

    T = 60 K
    
    \includegraphics[width=\textwidth]{Meth_sol}
    
    \smallskip
    
    $\left\langle V\right\rangle \simeq (\SI{47}{\angstrom})^3$

    \bigskip
    \includegraphics[width=\textwidth]{legion_romaine}
    \end{minipage}
    \begin{minipage}[t]{.325\textwidth}\centering
    \textbf{Liquide}

    T = 160 K
    
    \includegraphics[width=\textwidth]{Meth_liquid}
    
    \smallskip
    
    $\left\langle V\right\rangle \simeq (\SI{60}{\angstrom})^3$

    \bigskip
    \includegraphics[width=\textwidth]{bagarre}
    \end{minipage}
    \begin{minipage}[t]{.325\textwidth}\centering
    \textbf{Gaz}
    
    T = 400 K
    
    \includegraphics[width=\textwidth]{Meth_gaz}

    \smallskip
    
    $\left\langle V\right\rangle \simeq (\SI{160}{\angstrom})^3$
    
    \end{minipage}
    
    \bigskip
    \centering
    Calculation of structural data.
    \end{minipage}
\end{frame}

\begin{frame}
    \frametitle{Methane simulations}
    \framesubtitle{Let's melt a methane box}

    \centering
    \includegraphics[width=.9\textwidth]{melting}
    
\end{frame}


\begin{frame}
    \frametitle{Methane simulations}
    \framesubtitle{Thermodynamics quantities}

    \begin{center}
        \includegraphics[width=.85\textwidth]{isobare}    
    \end{center}
    
    Comparisons with experimental data:
    \begin{itemize}
        \item Validation of the simulations and the model
        \item Production/Predictions of data not available or difficult to obtain experimentally
    \end{itemize}
\end{frame}

\begin{frame}
    \frametitle{Methane simulations}
    \framesubtitle{Kinetic quantities}

    \begin{minipage}[c]{.55\textwidth}
        \includegraphics[width=\textwidth]{diffusion_coeff}      
    \end{minipage}
    \begin{minipage}[c]{.43\textwidth}
	    Calculation of the diffusion coefficient from the Einstein relation:
	    \begin{equation*}
	        6 D t = \langle\vert r(t) - r(0)\vert^2\rangle
	    \end{equation*}   
	    
	    \begin{itemize}
	        \item The diffusion coefficient is one of the transport coefficients
	        \item Be careful about the difference between diffusion and convection
	    \end{itemize}
    \end{minipage}

\end{frame}

\begin{frame}
    \frametitle{Conclusion}
    \framesubtitle{Take home message}

    \begin{block}{Computational chemistry and numerical simulations}
    
    \begin{itemize}
        \item A not well known discipline
        \item Somewhere between chemistry, physics and computer science
        \item Various domain of applications
    \end{itemize}
    
    \end{block}        
    
    \begin{exampleblock}{Molecular dynamics simulations}
    \begin{itemize}
        \item A straightforward application of the Newton's second law and point mechanic
        \item Easy to implement
        \item An easy introduction of the model notion
        \item A straightforward illustration of the link between intermolecular forces and the states of matter.
    \end{itemize}
    \end{exampleblock}

    {\setbeamercolor{block title}{fg=white, bg=Uvert}
    \setbeamercolor{block body}{bg=Uvert!10}
    \begin{block}{Which software ?}
    
    All software used in this presentation are free and open-source.
    \begin{itemize}
        \item VMD: Visual Molecular Dynamics \url{https://www.ks.uiuc.edu/Research/vmd/}
        \item LAMMPS Molecular Dynamics Simulator \url{https://lammps.sandia.gov/}
        \item GROMACS \url{http://www.gromacs.org/}
    \end{itemize}
    
    \end{block}}

\end{frame}

\include{hpc_en}

\thanksframe[%
    germain.vallverdu@univ-pau.fr\par
    http://gsalvatovallverdu.gitlab.io]
    {Thank you for your attention}
    
\end{document}

